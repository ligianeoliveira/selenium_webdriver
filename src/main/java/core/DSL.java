package core;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import java.util.ArrayList;
import java.util.List;
import static core.DriverFactory.getDriver;

public class DSL {

    /********* TextField e TextArea ************/

    public void escreve(String id_campo, String texto){
        escreveGenerico(By.id(id_campo),texto);
    }

    public void escreveGenerico(By by, String texto){
        getDriver().findElement(by).clear();
        getDriver().findElement(by).sendKeys(texto);
    }

    public String obterValorCampo(String id_campo) {
        return getDriver().findElement(By.id(id_campo)).getAttribute("value");
    }

    /********* Radio e Check ************/

    public void clicarRadioButtonCheckbox(String id){
        clicarRadioButtonCheckbox(By.id(id));
    }

    public void clicarRadioButtonCheckbox(By by){
        getDriver().findElement(by).click();
    }

    public boolean isSelectec(String id){
        return getDriver().findElement(By.id(id)).isSelected();
    }

    /********* Combo ************/

    public void selecionarCombo(String id, String valor) {
        WebElement element = getDriver().findElement(By.id(id));
        Select combo = new Select(element);
        combo.selectByVisibleText(valor);
    }

    public void deselecionarCombo(String id, String valor) {
        WebElement element = getDriver().findElement(By.id(id));
        Select combo = new Select(element);
        combo.deselectByVisibleText(valor);
    }

    public List<String> obterValoresCombo(String id){
        WebElement element = getDriver().findElement(By.id(id));
        Select combo = new Select(element);
        //Percorrer a combo e verificar quantos elementos estão selecionados
        List<WebElement> allSelectedOptions = combo.getAllSelectedOptions();
        List<String> valores = new ArrayList<String>();
        for(WebElement opcao: allSelectedOptions) {
            valores.add(opcao.getText());
        }
        return valores;
    }

    public String obterValorCombo(String id) {
        WebElement element = getDriver().findElement(By.id(id));
        Select combo = new Select(element);
        return combo.getFirstSelectedOption().getText();
    }

    public int obterQuantidadeOpcoesCombo(String id){
        WebElement element = getDriver().findElement(By.id(id));
        Select combo = new Select(element);
        //Percorrer a combo e verificar quantos elementos estão selecionados
        List<WebElement> options = combo.getOptions();
        return options.size();
    }

    public boolean verificarOpcaoCombo(String id, String opcao){
        WebElement element = getDriver().findElement(By.id(id));
        Select combo = new Select(element);
        //Percorrer a combo e verificar quantos elementos estão selecionados
        List<WebElement> options = combo.getOptions();
        for(WebElement option: options) {
            if(option.getText().equals(opcao)){
                return true;
            }
        }
        return false;
    }

    public void selecionarComboPrime(String radical, String valor){
        clicarRadioButtonCheckbox(By.xpath("//*[@id='"+radical+"_input']/../..//span"));
        try {
            new Thread().sleep(1000); //Esse valor é um milisegundos
        } catch (InterruptedException e) { e.printStackTrace(); }
        clicarRadioButtonCheckbox(By.xpath("//*[@id='"+radical+"_items']//li[.='"+valor+"']"));

    }

    /********* Botao ************/

    public void clicarBotao(String id){
        getDriver().findElement(By.id(id)).click();

    }

    public String obterValueElemento(String id) {
        return getDriver().findElement(By.id(id)).getAttribute("value");
    }

    /********* Link ************/

    public void clicarLink(String id){
        getDriver().findElement(By.linkText(id)).click();
    }

    /********* Textos ************/

    public String obterTexto(By by){
        return getDriver().findElement(by).getText();
    }

    public String obterTexto(String id){
        return obterTexto(By.id(id));
    }

    /********* Alerts ************/

    public String alertaObterTexto(){
        //muda o foco da tela para o alert e cria um objeto do tipo Alert para podermos manipulá-lo
        Alert alert = getDriver().switchTo().alert();
        //pega o texto exibido no alerta e retorna o mesmo
        return alert.getText();
    }

    public String alertaObterTextoEAceita(){
        //muda o foco da tela para o alert e cria um objeto do tipo Alert para podermos manipulá-lo
        Alert alert = getDriver().switchTo().alert();
        //pega o texto exibido no alerta
        String valor = alert.getText();
        alert.accept();
        return valor;
    }

    public String alertaObterTextoENega(){
        //muda o foco da tela para o alert e cria um objeto do tipo Alert para podermos manipulá-lo
        Alert alert = getDriver().switchTo().alert();
        //pega o texto exibido no alerta
        String valor = alert.getText();
        //seleciona a opção cancelar no alerta
        alert.dismiss();
        return valor;
    }

    public void alertaEscrever(String valor) {
        //muda o foco da tela para o alert e cria um objeto do tipo Alert para podermos manipulá-lo
        Alert alert = getDriver().switchTo().alert();
        //escreve no alerta
        alert.sendKeys(valor);
        alert.accept();
    }

    /********* Frames e Janelas ************/

    public void entrarFrame(String id) {
        //joga o foco para o frame
        getDriver().switchTo().frame(id);
    }

    public void sairFrame(){
        //Volta o foco para a página principal
        getDriver().switchTo().defaultContent();
    }

    public void trocarJanela(String id) {
        getDriver().switchTo().window(id);
    }

    /************** JS ****************/

    public Object executarJS(String cmd, Object... param){
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        return js.executeScript(cmd, param);
    }

    /************ Tabela *************/

    public void clicarBotaoTabela(String colunaBusca, String valor, String colunaBotao, String idTabela){

        //procurar coluna do registro
        WebElement tabela = getDriver().findElement(By.xpath("//*[@id='elementosForm:tableUsuarios']"));
        int idColuna = obterIndiceColuna(colunaBusca, tabela);

        //encontrar a linha do registro
        int idLinha = obterIndiceLinha(valor, tabela, idColuna);

        //procurar coluna do botão
        int idColunaBotao = obterIndiceColuna(colunaBotao, tabela);

        //clicar no botão da celula encontrada
        WebElement celula = tabela.findElement(By.xpath(".//tr["+idLinha+"]/td["+idColunaBotao+"]"));
        celula.findElement(By.xpath(".//input")).click();
    }

    private int obterIndiceLinha(String valor, WebElement tabela, int idColuna) {
        List<WebElement> linhas = tabela.findElements(By.xpath("./tbody/tr/td["+idColuna+"]"));
        int idLinha = -1;
        for (int i=0; i< linhas.size(); i++){
            if(linhas.get(i).getText().equals(valor)){
                idLinha = i+1;
                break;
            }
        }
        return idLinha;
    }

    private int obterIndiceColuna(String coluna, WebElement tabela) {
        List<WebElement> colunas = tabela.findElements(By.xpath(".//th"));
        int idColuna = -1;
        for (int i=0; i< colunas.size(); i++){
            if(colunas.get(i).getText().equals(coluna)){
                idColuna = i+1;
                break;
            }
        }
        return idColuna;
    }
}
