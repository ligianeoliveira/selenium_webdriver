package page;

import core.BasePage;
import org.openqa.selenium.By;

public class CampoTreinamentoPage extends BasePage {

    public void setNome(String nome){
        dsl.escreve("elementosForm:nome", nome );
    }
    public void setSobrenome(String sobrenome){
        dsl.escreve("elementosForm:sobrenome", sobrenome);
    }
    public void setSexoFeminino(){
        dsl.clicarRadioButtonCheckbox("elementosForm:sexo:1");
    }
    public void setSexoMasculino(){
        dsl.clicarRadioButtonCheckbox("elementosForm:sexo:0");
    }
    public void setComidaCarne(){
        dsl.clicarRadioButtonCheckbox("elementosForm:comidaFavorita:0");
    }
    public void setComidaVegetariano(){
        dsl.clicarRadioButtonCheckbox("elementosForm:comidaFavorita:3");
    }
    public void setEscolaridade(String valor){
        dsl.selecionarCombo("elementosForm:escolaridade",valor);
    }
    public void setEsporte(String... valores){
        for(String valor: valores)
        dsl.selecionarCombo("elementosForm:esportes",valor);
    }
    public void cadastrar(){
        dsl.clicarBotao("elementosForm:cadastrar");
    }
    public String obterResultadoCadastro(){
        return dsl.obterTexto(By.xpath("//*[@id='resultado']/span"));
    }
    public String obterNomeCadastrado(){
        return dsl.obterTexto(By.xpath("//*[@id='descNome']/span"));
    }
    public String obterSobrenomeCadastrado(){
        return dsl.obterTexto(By.xpath("//*[@id='descSobrenome']/span"));
    }
    public String obterSexoCadastrado(){
        return dsl.obterTexto(By.xpath("//*[@id='descSexo']/span"));
    }
    public String obterComidaCadastrado(){
        return dsl.obterTexto(By.xpath("//*[@id='descComida']/span"));
    }
    public String obterEscolaridadeCadastrado(){
        return dsl.obterTexto(By.xpath("//*[@id='descEscolaridade']/span"));
    }
    public String obterEsporteCadastrado(){
        return dsl.obterTexto(By.xpath("//*[@id='descEsportes']/span"));
    }
}
