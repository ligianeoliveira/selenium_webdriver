package test;

import core.DSL;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static core.DriverFactory.getDriver;
import static core.DriverFactory.killDriver;

public class TesteFramesEJanelas {

    private DSL dsl;

    @Before
    public void inicializa(){
        getDriver().get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
        dsl = new DSL();
    }

    @After
    public void finaliza(){
        killDriver();
    }

    @Test
    public void interagirComFrames() {
        dsl.entrarFrame("frame1");
        dsl.clicarBotao("frameButton");
        String msg = dsl.alertaObterTextoEAceita();
        Assert.assertEquals("Frame OK!", msg);
        dsl.sairFrame();
        dsl.escreve("elementosForm:nome", msg);
    }

    @Test
    public void interagirComJanelas() {
        dsl.clicarBotao("buttonPopUpEasy");
        dsl.trocarJanela("Popup");
        dsl.escreveGenerico(By.tagName("textarea"),"Deu certo?");
        getDriver().close();
        dsl.trocarJanela("");
        dsl.escreveGenerico(By.tagName("textarea"),"e?");
    }

    @Test
    public void interagirComJanelasSemTitulo() {
        dsl.clicarBotao("buttonPopUpHard");
        System.out.println(getDriver().getWindowHandles());
        //Vai para a janela que quer interagir e não sabe o nome
        dsl.trocarJanela((String)getDriver().getWindowHandles().toArray()[1]);
        dsl.escreveGenerico(By.tagName("textarea"),"Deu certo?");
        //Volta para a janela principal
        dsl.trocarJanela((String) getDriver().getWindowHandles().toArray()[0]);
        dsl.escreveGenerico(By.tagName("textarea"),"E agora?");
    }

    @Test
    public void interagirComFrameEscondido(){
        //java script utulizado para dar um scrool na tela para achar o botão do frame
        WebElement frame = getDriver().findElement(By.id("frame2"));
        dsl.executarJS("window.scrollBy(0, arguments[0])", frame.getLocation().y);
        dsl.entrarFrame("frame2");
        dsl.clicarBotao("frameButton");
        String msg = dsl.alertaObterTextoEAceita();
        Assert.assertEquals("Frame OK!", msg);

    }

}
