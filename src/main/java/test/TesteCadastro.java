package test;

import core.BaseTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import page.CampoTreinamentoPage;
import static core.DriverFactory.getDriver;


public class TesteCadastro extends BaseTest {

    private CampoTreinamentoPage page;

    @Before
    public void inicializa(){
        getDriver().get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
        page = new CampoTreinamentoPage();
    }

    @Test
    public void preencherFormularioComSucesso() {
        //preenchendo os dados
        page.setNome("Ligiane");
        page.setSobrenome("Oliveira");
        page.setSexoFeminino();
        page.setComidaCarne();
        page.setEscolaridade("Superior");
        page.setEsporte("Natacao");
        //cliando no botão Cadastrar
        page.cadastrar();
        //conferindo os dados
        Assert.assertEquals("Cadastrado!", page.obterResultadoCadastro());
        Assert.assertEquals("Ligiane", page.obterNomeCadastrado());
        Assert.assertEquals("Oliveira", page.obterSobrenomeCadastrado());
        Assert.assertEquals("Feminino", page.obterSexoCadastrado());
        Assert.assertEquals("Carne", page.obterComidaCadastrado());
        Assert.assertEquals("superior", page.obterEscolaridadeCadastrado());
        Assert.assertEquals("Natacao", page.obterEsporteCadastrado());
    }
}
