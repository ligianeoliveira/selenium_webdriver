package test;

import core.DSL;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import static core.DriverFactory.getDriver;
import static core.DriverFactory.killDriver;

public class TestePrime {

    private DSL dsl;

    @Before
    public void inicializa(){
        dsl = new DSL();
    }

    @After
    public void finaliza(){
        killDriver();
    }

    @Test
    public void deveInteragirComRadioPrime(){
        getDriver().get("https://www.primefaces.org/showcase/ui/input/oneRadio.xhtml");

        dsl.clicarRadioButtonCheckbox(By.xpath("//input[@id='j_idt698:console:0']/../..//span"));
        Assert.assertTrue(dsl.isSelectec("j_idt698:console:0"));
        dsl.clicarRadioButtonCheckbox(By.xpath("//label[.='PS4']/..//span"));
        Assert.assertTrue(dsl.isSelectec("j_idt698:console:1"));
    }

    @Test
    public void deveInteragirComSelectPrime(){
        getDriver().get("https://www.primefaces.org/showcase/ui/input/oneMenu.xhtml");

        dsl.selecionarComboPrime("j_idt698:console", "Xbox One");
        Assert.assertEquals("Xbox One", dsl.obterTexto("j_idt698:console_label"));
    }










}
