package test;

import core.DSL;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.Arrays;
import java.util.List;

import static core.DriverFactory.getDriver;
import static core.DriverFactory.killDriver;

public class TesteCampoTreinamento {

    private DSL dsl;

    @Before
    public void inicializa(){
        getDriver().get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
        dsl = new DSL();
    }

    @After
    public void finaliza(){
        killDriver();
    }

    @Test
    public void testeTextFild() {
        dsl.escreve("elementosForm:nome", "Teste de escrita");
        Assert.assertEquals("Teste de escrita", dsl.obterValorCampo("elementosForm:nome"));
    }

    @Test
    public void testeTextFieldDuplo(){
        dsl.escreveGenerico(By.id("elementosForm:nome"), "Ligiane");
        Assert.assertEquals("Ligiane", dsl.obterValorCampo("elementosForm:nome"));
        dsl.escreveGenerico(By.id("elementosForm:nome"), "Mara");
        Assert.assertEquals("Mara", dsl.obterValorCampo("elementosForm:nome"));
    }

    @Test
    public void testeTextArea() {
        dsl.escreve("elementosForm:sugestoes", "teste");
        Assert.assertEquals("teste", dsl.obterValorCampo("elementosForm:sugestoes"));
    }

    @Test
    public void testeRadioButton() {
        dsl.clicarRadioButtonCheckbox("elementosForm:sexo:0");
        Assert.assertTrue(dsl.isSelectec("elementosForm:sexo:0"));
    }

    @Test
    public void testeCheckBox() {
        dsl.clicarRadioButtonCheckbox("elementosForm:comidaFavorita:1");
        Assert.assertTrue(dsl.isSelectec("elementosForm:comidaFavorita:1"));
    }

    @Test
    public void testeCombo() {
        dsl.selecionarCombo("elementosForm:escolaridade", "2o grau completo");
        Assert.assertEquals("2o grau completo", dsl.obterValorCombo("elementosForm:escolaridade"));
    }

    @Test
    public void testeVerificarValoresCombo() {
        Assert.assertEquals(8, dsl.obterQuantidadeOpcoesCombo("elementosForm:escolaridade"));
        Assert.assertTrue(dsl.verificarOpcaoCombo("elementosForm:escolaridade", "Mestrado"));
    }

    @Test
    public void testeVerificarValoresComboMultiplo() {
        dsl.selecionarCombo("elementosForm:esportes","Natacao" );
        dsl.selecionarCombo("elementosForm:esportes","Corrida" );
        dsl.selecionarCombo("elementosForm:esportes","O que eh esporte?" );

        List<String> opcoesMarcadas = dsl.obterValoresCombo("elementosForm:esportes");
        Assert.assertEquals(3, opcoesMarcadas.size());

        dsl.deselecionarCombo("elementosForm:esportes", "Corrida");
        opcoesMarcadas = dsl.obterValoresCombo("elementosForm:esportes");
        Assert.assertEquals(2, opcoesMarcadas.size());
        Assert.assertTrue(opcoesMarcadas.containsAll(Arrays.asList("Natacao", "O que eh esporte?")));
    }

    @Test
    public void testeBotoes(){
        dsl.clicarBotao("buttonSimple");
        Assert.assertEquals("Obrigado!", dsl.obterValueElemento("buttonSimple"));
    }

    @Test
    public void testeLinks() {
        dsl.clicarLink("Voltar");
        Assert.assertEquals("Voltou!", dsl.obterTexto("resultado"));
        //Assertions.fail();
    }

    @Test
    public void testeTextoNaTela() {
        Assert.assertEquals("Campo de Treinamento", dsl.obterTexto(By.tagName("h3")));
        Assert.assertEquals("Cuidado onde clica, muitas armadilhas...", dsl.obterTexto(By.className("facilAchar")));
    }

    @Test
    public void testJavascript(){
        //js.executeScript("alert('Testando js via selenium')");
        dsl.executarJS("document.getElementById('elementosForm:nome').value='Escrito via js'");
        dsl.executarJS("document.getElementById('elementosForm:sobrenome').type='radio'");
        WebElement element = getDriver().findElement(By.id("elementosForm:nome"));
        dsl.executarJS("arguments[0].style.border=arguments[1]", element, "solid 4px red");
    }

    @Test
    public void deveClicarBotaoTabela(){
        dsl.clicarBotaoTabela("Nome", "Maria", "Botao", "elementosForm:tableUsuarios");
    }

}
